# ra_wCo_din_g vue2 dotn_et core
link
![](images/2023-05-17-09-27-41.png)
<!--

vue 2  
![](images/2023-05-17-07-42-52.png)

example import cdn  
![](images/2023-05-17-07-46-32.png)

example input vue cdn  
![](images/2023-05-17-07-47-05.png)

v-bind  
![](images/2023-05-17-07-48-17.png)
![](images/2023-05-17-07-48-26.png)

input user pass  
![](images/2023-05-17-07-49-12.png)
![](images/2023-05-17-07-49-20.png)

loginForm method  
![](images/2023-05-17-07-52-09.png)
![](images/2023-05-17-07-52-19.png)

bind this  
![](images/2023-05-17-07-53-27.png)
![](images/2023-05-17-07-53-36.png)

npm install  
![](images/2023-05-17-08-01-46.png)

index.html hello world  
![](images/2023-05-17-08-02-14.png)


--> 

vue3 install globally  
![](images/2023-05-17-08-37-26.png)

vue 3.5.5 install globally  
![](images/2023-05-17-08-37-50.png)

vue cli  
![](images/2023-05-17-08-38-09.png)

vue create new app  
![](images/2023-05-17-08-38-55.png)

![](images/2023-05-17-08-39-05.png)

manually select features  
![](images/2023-05-17-08-39-38.png)

use pre-processor css  
![](images/2023-05-17-08-40-28.png)

use stylus preprocessor css  
![](images/2023-05-17-08-40-46.png)

eslint setup  
![](images/2023-05-17-08-41-10.png)

lint on save  
![](images/2023-05-17-08-41-34.png)

config in package.json  
![](images/2023-05-17-08-42-01.png)

don't save as template  
![](images/2023-05-17-08-42-22.png)

all done  
![](images/2023-05-17-08-42-48.png)

serve  
![](images/2023-05-17-08-52-11.png) 
![](images/2023-05-17-08-51-59.png)

remove all template stuff  
![](images/2023-05-17-08-53-56.png)

input form  
![](images/2023-05-17-08-54-57.png)

add v-model to input form  
![](images/2023-05-17-08-55-23.png)
![](images/2023-05-17-08-55-31.png)
![](images/2023-05-17-08-55-42.png)

add validation  
![](images/2023-05-17-09-25-20.png)

for validation textLimit and inputCount()  
![](images/2023-05-17-09-26-24.png)
![](images/2023-05-17-09-26-37.png)

console log the result  
![](images/2023-05-17-09-27-06.png)
![](images/2023-05-17-09-27-16.png)

set length limit  
![](images/2023-05-18-19-41-48.png)

isEmpty method  
![](images/2023-05-18-19-43-05.png)

validationClass directive set  
![](images/2023-05-18-19-44-03.png)

![](images/2023-05-18-19-44-24.png)

components/TextField.vue  
![](images/2023-05-18-19-50-48.png)

refactor input from App.vue  
![](images/2023-05-18-19-51-20.png)

copy all methods to TextField.vue  
![](images/2023-05-18-19-52-27.png)
![](images/2023-05-18-19-52-40.png)

@input directive  
![](images/2023-05-18-20-10-42.png)

event bind to input  
![](images/2023-05-18-20-11-11.png)
![](images/2023-05-18-20-11-25.png)

change watch  
![](images/2023-05-18-20-11-51.png) 

data  
![](images/2023-05-18-20-12-06.png)

add computed method  
![](images/2023-05-18-20-12-30.png)

add label   
![](images/2023-05-18-20-12-49.png)

bind label directive  
![](images/2023-05-18-20-13-14.png)